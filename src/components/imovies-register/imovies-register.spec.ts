import { TestWindow } from '@stencil/core/testing';
import { ImoviesRegister } from './imovies-register';

describe('imovies-register', () => {
  it('should build', () => {
    expect(new ImoviesRegister()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLImoviesRegisterElement;
    let testWindow: TestWindow;
    beforeEach(async () => {
      testWindow = new TestWindow();
      element = await testWindow.load({
        components: [ImoviesRegister],
        html: '<imovies-register></imovies-register>'
      });
    });

    // See https://stenciljs.com/docs/unit-testing
    {cursor}

  });
});
