import { Component, State, Prop } from "@stencil/core";
import DatabaseProvider from "../../services/database";

@Component({
  tag: "imovies-register",
  styleUrl: "imovies-register.scss"
})
export class ImoviesRegister {
  @Prop({ connect: "ion-nav" })
  nav: HTMLIonNavElement;
  @Prop({ connect: "ion-loading-controller" })
  loadingCtrl: HTMLIonLoadingControllerElement;
  @Prop({ connect: "ion-toast-controller" })
  toastCtrl: HTMLIonToastControllerElement;
  private databaseProvider: DatabaseProvider = new DatabaseProvider();
  @State()
  name: string;
  @State()
  username: string;
  @State()
  email: string;
  @State()
  password: string;

  register() {
    if (
      this.name != null &&
      this.username != null &&
      this.email != null &&
      this.password != null
    ) {
      this.loadingCtrl
        .create({
          spinner: "dots",
          message: "Registering...",
          translucent: true,
          cssClass: "loader"
        })
        .then(loader => {
          loader.present();
          this.databaseProvider
            .registerWithEmail(
              this.email,
              this.password,
              this.name,
              this.username
            )
            .then(() => {
              loader.dismiss();
              this.toastCtrl
                .create({
                  message:
                    "Successfully registered. Check your email to verify your account",
                  showCloseButton: true,
                  position: "top",
                  closeButtonText: "Okay",
                  cssClass: "toast-success"
                })
                .then(toast => {
                  toast.present();
                  toast.onDidDismiss().then(() => {
                    this.nav.componentOnReady().then(navEl => {
                      navEl.setRoot("imovies-login");
                    });
                  });
                })
                .catch(error => {
                  console.error(error);
                });
            })
            .catch(error => {
              loader.dismiss();
              console.error(error);
              if (error == "email") {
                this.toastCtrl
                  .create({
                    message: "Email is already registered",
                    showCloseButton: true,
                    position: "top",
                    closeButtonText: "Okay",
                    cssClass: "toast-fail"
                  })
                  .then(toast => {
                    toast.present();
                  })
                  .catch(error => {
                    console.error(error);
                  });
              } else {
                this.toastCtrl
                  .create({
                    message: "There was an error during registration",
                    showCloseButton: true,
                    position: "top",
                    closeButtonText: "Okay",
                    cssClass: "toast-fail"
                  })
                  .then(toast => {
                    toast.present();
                  })
                  .catch(error => {
                    console.error(error);
                  });
              }
            });
        })
        .catch(error => {
          console.error(error);
        });
    } else {
      this.toastCtrl
        .create({
          message: "Please fill all fields",
          showCloseButton: true,
          position: "top",
          closeButtonText: "Okay",
          cssClass: "toast-fail"
        })
        .then(toast => {
          toast.present();
        })
        .catch(error => {
          console.error(error);
        });
    }
  }
  render() {
    return (
      <ion-split-pane when="lg">
        <ion-content main>
          <div class="login-form">
            <h4>Register</h4>
            <ion-item>
              <ion-label position="stacked">Name</ion-label>
              <ion-input
                type="text"
                autofocus
                clearInput
                placeholder="Enter Your Full Name"
                value={this.name}
                onInput={(event: any) => {
                  this.name = event.target.value;
                }}
              />
            </ion-item>
            <ion-item>
              <ion-label position="stacked">Username</ion-label>
              <ion-input
                type="text"
                clearInput
                placeholder="Enter Desired User Name"
                value={this.username}
                onInput={(event: any) => {
                  this.username = event.target.value;
                }}
              />
            </ion-item>
            <ion-item>
              <ion-label position="stacked">Email</ion-label>
              <ion-input
                type="email"
                clearInput
                inputmode="email"
                placeholder="Enter Your Email"
                value={this.email}
                onInput={(event: any) => {
                  this.email = event.target.value;
                }}
              />
            </ion-item>
            <ion-item>
              <ion-label position="stacked">Password</ion-label>
              <ion-input
                type="password"
                placeholder="Enter A Password"
                value={this.password}
                onInput={(event: any) => {
                  this.password = event.target.value;
                }}
              />
            </ion-item>

            <ion-button
              color="primary"
              onClick={() => {
                this.register();
              }}
            >
              Register
            </ion-button>

            <ion-anchor href="/login" routerDirection="forward">
              Already Have An Account?
            </ion-anchor>
          </div>
        </ion-content>
        <div class="sideEf">
          <div class="title">
            <h2>iMovies</h2>
            <p>Movie Booking With Ease</p>
          </div>
        </div>
      </ion-split-pane>
    );
  }
}
