import { Component, Prop } from "@stencil/core";

@Component({
  tag: "imovies-film-card",
  styleUrl: "imovies-film-card.scss"
})
export class ImoviesFilmCard {
  @Prop()
  movie: any;

  render() {
    return (
      <div class="film-card">
        <img
          src={"https://image.tmdb.org/t/p/w342" + this.movie.poster_path}
          alt={this.movie.title}
        />
        <div class="info">
          <h3>{this.movie.title}</h3>
          <p>
            <span class="bold">Overview: </span>
            {this.movie.overview}
          </p>
          <p>
            <span class="bold">Runtime: </span>
            {this.movie.runtime} minutes
          </p>
          <p>
            <span class="bold">Genres: </span>
            {this.movie.genres
              ? this.movie.genres.map(genre => <span>{genre.name},</span>)
              : null}
          </p>

          <p>
            <span class="bold">Realeased: </span>
            {this.movie.release_date}
          </p>
          <p>
            <span class="bold">Rating: </span>
            {this.movie.vote_average}
          </p>

          <div class="divider">
            <hr />
            <ion-button href={"/movie/" + this.movie.id}>BUY TICKET</ion-button>
            <hr />
          </div>
        </div>
      </div>
    );
  }
}
