import { Component, Prop, Listen, State } from "@stencil/core";
import { FirebaseConfig } from "../../helpers/firebase.config";
import firebase from "firebase";

@Component({
  tag: "app-root",
  styleUrl: "app-root.scss"
})
export class AppRoot {
  @Prop({ connect: "ion-toast-controller" })
  toastCtrl: HTMLIonToastControllerElement;

  @State()
  user: firebase.User;

  /**
   * Handle service worker updates correctly.
   * This code will show a toast letting the
   * user of the PWA know that there is a
   * new version available. When they click the
   * reload button it then reloads the page
   * so that the new service worker can take over
   * and serve the fresh content
   */
  @Listen("window:swUpdate")
  async onSWUpdate() {
    const toast = await this.toastCtrl.create({
      message: "New version available",
      showCloseButton: true,
      closeButtonText: "Reload"
    });
    await toast.present();
    await toast.onWillDismiss();
    window.location.reload();
  }

  componentWillLoad() {
    this.configureFirebase();
  }

  configureFirebase() {
    firebase.initializeApp(FirebaseConfig);
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL);
    firebase.firestore().settings({ timestampsInSnapshots: true });
    firebase
      .firestore()
      .enablePersistence()
      .catch(err => {
        console.error(err);
      });
  }

  render() {
    return (
      <ion-app>
        <ion-router useHash={false}>
          <ion-route url="/" component="imovies-start" />
          <ion-route url="/home" component="imovies-home" />
          <ion-route url="/login" component="imovies-login" />
          <ion-route url="/register" component="imovies-register" />
          <ion-route url="/movie/:movieid" component="imovies-film" />
          <ion-route url="/profile/:name" component="imovies-profile" />
        </ion-router>
        <ion-nav />
      </ion-app>
    );
  }
}
