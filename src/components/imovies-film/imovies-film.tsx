import { Component, Prop, State } from "@stencil/core";
import DatabaseProvider from "../../services/database";
import MoviesProvider from "../../services/movies";

@Component({
  tag: "imovies-film",
  styleUrl: "imovies-film.scss"
})
export class ImoviesFilm {
  @Prop({ connect: "ion-nav" })
  nav: HTMLIonNavElement;
  @Prop({ connect: "ion-loading-controller" })
  loadingCtrl: HTMLIonLoadingControllerElement;
  @Prop({ connect: "ion-toast-controller" })
  toastCtrl: HTMLIonToastControllerElement;
  @Prop({ connect: "ion-alert-controller" })
  alertCtrl: HTMLIonAlertControllerElement;
  private databaseProvider: DatabaseProvider = new DatabaseProvider();
  private moviesProvider: MoviesProvider = new MoviesProvider();
  @Prop()
  movieid: any;
  @State()
  movie: any;
  @State()
  cast: Array<any> = [];
  @State()
  movieTheatres: Array<any> = [];
  @State()
  movieTimes: Array<any> = [];
  @State()
  movieDates: Array<any> = [];
  @State()
  movieSeats: Array<any> = [];
  @State()
  showSeats: Boolean = false;
  currentDate = null;
  selectedSeats: Array<any> = [];
  @State()
  currentTheatre = null;

  componentWillLoad() {
    this.checkAuth();
  }

  componentDidLoad() {
    this.getFilmDetails();
    this.movieTheatres = this.moviesProvider.getRandomTheatre();
    var index = 0;
    do {
      let date = this.moviesProvider.getrandomDate().toDateString();
      if (!this.movieDates.includes(date)) {
        this.movieDates = [...this.movieDates, date];
        index++;
      }
    } while (index < 5);
  }
  getFilmDetails() {
    this.moviesProvider
      .getMovieDetails(this.movieid)
      .then(movie => {
        this.movie = movie;
        this.moviesProvider
          .getMovieCast(this.movie.id)
          .then((cast: any) => {
            this.cast = cast;
          })
          .catch(error => {
            console.error(error);
          });
      })
      .catch(error => {
        console.error(error);
      });
  }
  checkAuth() {
    //private databaseProvider: DatabaseProvider = new DatabaseProvider();
    this.databaseProvider.checkLoginStatus().subscribe(
      (user: firebase.User) => {
        user = user;
      },
      error => {
        console.error(error);
        this.nav.componentOnReady().then(navEl => {
          navEl.setRoot("imovies-start");
        });
      }
    );
  }

  getShowTimes() {
    let cinemas = [];
    this.moviesProvider.getRandomTheatre().map(theatre => {
      let cinema = { theatre: theatre, times: [] };
      for (let index = 0; index < 4; index++) {
        cinema.times.push({
          time:
            this.moviesProvider.getrandomDate().getHours() +
            ":" +
            ("0" + this.moviesProvider.getrandomDate().getMinutes()).substr(-2),
          type: this.moviesProvider.getViewingType()
        });
      }
      cinemas.push(cinema);
    });
    this.movieTimes = cinemas;
  }

  getSeats(theatre, time) {
    this.movieSeats = [];
    this.currentTheatre = { theatre: theatre, time: time };

    this.loadingCtrl
      .create({
        spinner: "dots",
        message: "Getting Seats",
        translucent: true,
        cssClass: "loader"
      })
      .then(loader => {
        loader.present();
        this.databaseProvider
          .getBookedSeats(this.movieid, theatre, time, this.currentDate)
          .subscribe(
            (res: Array<any>) => {
              if (res.length > 0) {
                let seats = this.moviesProvider.getSeats();
                let letters = ["a", "b", "c", "d", "e", "f"];

                var count = 0;

                for (const item of res) {
                  let row = item.seat.split(/([0-9]+)/)[0];
                  let column = parseInt(item.seat.split(/([0-9]+)/)[1]);
                  seats[letters.indexOf(row)].columns[column].booked = true;
                  if ((count = res.length)) {
                    this.movieSeats = seats;
                    loader.dismiss();
                  }
                  count++;
                }
              } else {
                this.movieSeats = this.moviesProvider.getSeats();
                loader.dismiss();
              }
            },
            error => {
              console.error(error);
            }
          );
      });
  }

  bookSeats() {
    if (this.selectedSeats.length > 0) {
      this.alertCtrl
        .create({
          header: "Finalise!",
          message:
            "Book Tickets for Seats: " + this.selectedSeats.toString() + "",
          backdropDismiss: false,
          animated: true,
          buttons: [
            {
              text: "Cancel",
              role: "cancel",
              cssClass: "secondary",
              handler: () => {}
            },
            {
              text: "Book",
              handler: () => {
                this.loadingCtrl
                  .create({
                    spinner: "dots",
                    message: "Booking Your Seats",
                    translucent: true,
                    cssClass: "loader"
                  })
                  .then(loader => {
                    loader.present();
                    this.databaseProvider
                      .bookMovie(
                        this.movieid,
                        this.currentTheatre.theatre,
                        this.currentTheatre.time,
                        this.currentDate,
                        this.selectedSeats
                      )
                      .then(() => {
                        loader.dismiss();
                        this.toastCtrl
                          .create({
                            message:
                              "Successfully booked. You will receive tickets in your email address once processed",
                            showCloseButton: true,
                            position: "top",
                            closeButtonText: "Okay",
                            cssClass: "toast-success"
                          })
                          .then(toast => {
                            toast.present();
                          })
                          .catch(error => {
                            console.error(error);
                          });
                        this.movieSeats = [];
                        this.showSeats = false;
                        this.currentTheatre = null;
                        this.selectedSeats = [];
                      })
                      .catch(error => {
                        loader.dismiss();
                        console.error(error);
                        this.toastCtrl
                          .create({
                            message: "There was an error booking your seats",
                            showCloseButton: true,
                            position: "top",
                            closeButtonText: "Okay",
                            cssClass: "toast-fail"
                          })
                          .then(toast => {
                            toast.present();
                          })
                          .catch(error => {
                            console.error(error);
                          });
                      });
                  });
              }
            }
          ]
        })
        .then(alert => {
          alert.present();
        });
    } else {
      this.alertCtrl
        .create({
          header: "Incomplete",
          message: "Please select at least one seat",
          buttons: ["Ok"],
          backdropDismiss: true
        })
        .then(alert => {
          alert.present();
        });
    }
  }

  render() {
    return (
      <ion-content>
        <imovies-header />
        <div class="body" padding>
          {this.movie != null ? (
            <div class="film-card">
              <img
                src={"https://image.tmdb.org/t/p/w342" + this.movie.poster_path}
                alt={this.movie.title}
              />
              <div class="info">
                <h3>{this.movie.title}</h3>
                <p>
                  <span class="bold">Overview: </span>
                  {this.movie.overview}
                </p>
                <p>
                  <span class="bold">Runtime: </span>
                  {this.movie.runtime} minutes
                </p>
                {this.cast.length > 0 ? (
                  <p>
                    <span class="bold">Cast: </span>
                    {this.cast.slice(0, 20).map(actor => (
                      <span>{actor.name}, </span>
                    ))}
                  </p>
                ) : null}
                <p>
                  <span class="bold">Genres: </span>
                  {this.movie.genres
                    ? this.movie.genres.map(genre => <span>{genre.name},</span>)
                    : null}
                </p>

                <p>
                  <span class="bold">Realeased: </span>
                  {this.movie.release_date}
                </p>
                <p>
                  <span class="bold">Rating: </span>
                  {this.movie.vote_average}
                </p>
              </div>
            </div>
          ) : null}
          {this.currentTheatre != null ? (
            <div class="currentTheatre">
              <h3>{this.currentTheatre.theatre}</h3>
              <h6>{this.currentTheatre.time}</h6>
            </div>
          ) : null}
          {this.movieSeats.length > 0 && this.showSeats ? (
            <div>
              <ion-grid class="seats">
                {this.movieSeats.map(row => (
                  <ion-row>
                    {row.columns.map(column => (
                      <ion-col>
                        <ion-checkbox
                          value={row.row + column.column}
                          checked={column.booked}
                          disabled={column.booked}
                          onIonChange={event => {
                            if (event.detail.checked) {
                              this.selectedSeats = [
                                ...this.selectedSeats,
                                event.detail.value
                              ];
                            } else {
                              this.selectedSeats.splice(
                                this.selectedSeats.indexOf(event.detail.value),
                                1
                              );
                            }
                          }}
                        />
                        <p>{row.row + column.column}</p>
                      </ion-col>
                    ))}
                  </ion-row>
                ))}
              </ion-grid>
              <ion-button
                onClick={() => {
                  this.bookSeats();
                }}
                expand="full"
                size="large"
              >
                BOOK SEATS
              </ion-button>
            </div>
          ) : null}

          {this.movieDates.length > 0 ? (
            <div>
              <h3>Choose A Date</h3>
              <ion-item class="dateselect">
                <ion-label>Movie Date</ion-label>
                <ion-select
                  id="customActionSheetSelect"
                  interface="action-sheet"
                  interfaceOptions={{
                    header: "Date",
                    subHeader: "Select a date"
                  }}
                  placeholder="Select One"
                  onIonChange={event => {
                    this.getShowTimes();
                    this.currentDate = event.detail.value.replace(/\s/g, "");
                  }}
                >
                  {this.movieDates.map(date => (
                    <ion-select-option value={date}>{date}</ion-select-option>
                  ))}
                </ion-select>
              </ion-item>
              <ion-grid>
                {this.movieTimes.map(movietime => (
                  <ion-row>
                    <ion-col size={"3"}>
                      <b>{movietime.theatre}</b>
                    </ion-col>
                    <ion-col class="right">
                      {movietime.times.map(time => (
                        <span
                          class="time"
                          onClick={() => {
                            var th = movietime.theatre.replace(/\s/g, "");
                            var ti = time.time.replace(/:/g, "") + time.type;

                            this.getSeats(th, ti);
                            this.showSeats = true;
                          }}
                        >
                          {time.time + "(" + time.type + ")"}
                        </span>
                      ))}
                    </ion-col>
                  </ion-row>
                ))}
              </ion-grid>
            </div>
          ) : null}
        </div>
      </ion-content>
    );
  }
}
