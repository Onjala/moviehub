import { TestWindow } from '@stencil/core/testing';
import { ImoviesFooter } from './imovies-footer';

describe('imovies-footer', () => {
  it('should build', () => {
    expect(new ImoviesFooter()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLImoviesFooterElement;
    let testWindow: TestWindow;
    beforeEach(async () => {
      testWindow = new TestWindow();
      element = await testWindow.load({
        components: [ImoviesFooter],
        html: '<imovies-footer></imovies-footer>'
      });
    });

    // See https://stenciljs.com/docs/unit-testing
    {cursor}

  });
});
