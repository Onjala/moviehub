import { Component } from "@stencil/core";

@Component({
  tag: "imovies-footer",
  styleUrl: "imovies-footer.scss"
})
export class ImoviesFooter {
  render() {
    return (
      <div>
        <p>Hello ImoviesFooter!</p>
      </div>
    );
  }
}
