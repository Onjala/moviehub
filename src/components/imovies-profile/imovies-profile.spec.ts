import { TestWindow } from '@stencil/core/testing';
import { ImoviesProfile } from './imovies-profile';

describe('imovies-profile', () => {
  it('should build', () => {
    expect(new ImoviesProfile()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLImoviesProfileElement;
    let testWindow: TestWindow;
    beforeEach(async () => {
      testWindow = new TestWindow();
      element = await testWindow.load({
        components: [ImoviesProfile],
        html: '<imovies-profile></imovies-profile>'
      });
    });

    // See https://stenciljs.com/docs/unit-testing
    {cursor}

  });
});
