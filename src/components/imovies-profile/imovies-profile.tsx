import { Component, Prop, State } from "@stencil/core";
import DatabaseProvider from "../../services/database";

@Component({
  tag: "imovies-profile",
  styleUrl: "imovies-profile.scss"
})
export class ImoviesProfile {
  @Prop({ connect: "ion-nav" })
  nav: HTMLIonNavElement;
  private databaseProvider: DatabaseProvider = new DatabaseProvider();
  @State()
  state = false;
  @Prop()
  name: string;
  @State()
  user: any;

  checkAuth() {
    //private databaseProvider: DatabaseProvider = new DatabaseProvider();
    this.databaseProvider.checkLoginStatus().subscribe(
      (user: firebase.User) => {
        user = user;
      },
      error => {
        this.nav.componentOnReady().then(navEl => {
          navEl.setRoot("imovies-start");
        });
      }
    );
  }
  componentWillLoad() {
    this.checkAuth();
  }
  componentDidLoad() {
    this.databaseProvider.getUserDetails().subscribe(
      user => {
        this.user = user;
      },
      error => {
        console.error(error);
      }
    );
  }
  render() {
    return (
      <ion-content>
        <imovies-header />
        {this.user != null ? (
          <div class="body">
            <div class="top">
              <img src={this.user.profilephoto} alt={this.user.userName} />
              <h2>{this.user.displayName}</h2>
              <h5>{this.user.userName}</h5>
              <ion-button
                shape="round"
                onClick={() => {
                  this.databaseProvider.logout();
                }}
              >
                Logout
              </ion-button>
            </div>
            <div class="bottom">
              <h2>iMovies</h2>
            </div>
          </div>
        ) : null}
      </ion-content>
    );
  }
}
