import { Component, State, Prop, EventEmitter, Event } from "@stencil/core";
import DatabaseService from "../../services/database";

@Component({
  tag: "imovies-header",
  styleUrl: "imovies-header.scss"
})
export class ImoviesHeader {
  @Prop({ connect: "ion-nav" })
  nav: HTMLIonNavElement;
  private databaseService: DatabaseService = new DatabaseService();
  @State()
  user: firebase.User;
  @Event()
  searchChanged: EventEmitter;
  @Event()
  searchCleared: EventEmitter;

  componentWillLoad() {
    this.databaseService.checkLoginStatus().subscribe(
      (user: firebase.User) => {
        this.user = user;
      },
      error => {
        console.error(error);
      }
    );
  }
  render() {
    return (
      <div class="site-header" color="primary">
        <h2>iMovies</h2>
        <div class="links">
          <a
            onClick={() => {
              this.nav.componentOnReady().then(navEl => {
                navEl.setRoot("imovies-home");
              });
            }}
          >
            HOME
          </a>
          <ion-searchbar
            placeholder="Search Movies"
            debounce={300}
            onIonChange={event => {
              this.searchChanged.emit(event);
            }}
            onIonClear={event => {
              this.searchCleared.emit(event);
            }}
          />
        </div>
        {this.user ? (
          <div
            class="user"
            onClick={() => {
              this.nav.componentOnReady().then(navEl => {
                navEl.push("imovies-profile", {
                  name: this.user.displayName.split(" ")[0]
                });
              });
            }}
          >
            <p>{this.user.displayName.split(" ")[0]}</p>
          </div>
        ) : null}
      </div>
    );
  }
}
