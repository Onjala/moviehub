import { Component, State, Prop } from "@stencil/core";
import MoviesProvider from "../../services/movies";

import DatabaseProvider from "../../services/database";

@Component({
  tag: "imovies-home",
  styleUrl: "imovies-home.scss"
})
export class ImoviesHome {
  @Prop({ connect: "ion-nav" })
  nav: HTMLIonNavElement;
  private databaseProvider: DatabaseProvider = new DatabaseProvider();
  private moviesProvider: MoviesProvider = new MoviesProvider();
  @State()
  nowPlayingMovies: Array<any> = [];
  nowPlayingMoviesfallback: Array<any> = [];
  @State()
  upcomingMovies: Array<any> = [];
  @State()
  currentPage: number = 1;
  @State()
  loading: Boolean = false;

  componentWillLoad() {
    this.checkAuth();
  }

  componentDidLoad() {
    this.getShowingMovies();
    this.getUpcomingMovies();
  }

  checkAuth() {
    //private databaseProvider: DatabaseProvider = new DatabaseProvider();
    this.databaseProvider.checkLoginStatus().subscribe(
      (user: firebase.User) => {
        user = user;
      },
      error => {
        this.nav.componentOnReady().then(navEl => {
          navEl.setRoot("imovies-start");
        });
      }
    );
  }

  getMoreShowingMovies() {
    this.currentPage++;
    this.loading = true;
    this.getShowingMovies(this.currentPage);
  }

  getShowingMovies(page: number = 1) {
    this.loading = true;
    this.moviesProvider
      .getShowingMovies(page)
      .then((movies: any) => {
        this.loading = false;
        for (const movie of movies) {
          this.moviesProvider
            .getMovieDetails(movie.id)
            .then(moviedetails => {
              this.nowPlayingMovies = [...this.nowPlayingMovies, moviedetails];
              this.nowPlayingMoviesfallback = [
                ...this.nowPlayingMoviesfallback,
                moviedetails
              ];
            })
            .catch(error => {
              console.error(error);
            });
        }
      })
      .catch(error => {
        this.loading = false;
        console.error(error);
      });
  }
  getUpcomingMovies(page: number = 1) {
    this.moviesProvider
      .getUpcomingMovies(page)
      .then((movies: any) => {
        this.upcomingMovies = movies;
      })
      .catch(error => {
        console.error(error);
      });
  }

  search(query) {
    this.moviesProvider
      .searchMovie(query, 1)
      .then((movies: any) => {
        if (movies != undefined) {
          this.nowPlayingMovies = [];
          for (const movie of movies) {
            this.moviesProvider
              .getMovieDetails(movie.id)
              .then(moviedetails => {
                this.nowPlayingMovies = [
                  ...this.nowPlayingMovies,
                  moviedetails
                ];
              })
              .catch(error => {
                console.error(error);
              });
          }
        }
      })
      .catch(error => {
        console.error(error);
      });
  }
  render() {
    return (
      <ion-content>
        <imovies-header
          onSearchChanged={event => {
            if (event.detail.detail.value.length > 0) {
              this.search(event.detail.detail.value);
            }
          }}
          onSearchCleared={() => {
            this.nowPlayingMovies = this.nowPlayingMoviesfallback;
          }}
        />
        <div class="body">
          <div class="left" padding>
            {this.nowPlayingMovies.length > 0 ? (
              <div class="movies">
                <h4>Now Playing Movies</h4>
                <div class="movie-cards">
                  {this.nowPlayingMovies.map(movie => (
                    <imovies-film-card movie={movie} />
                  ))}
                </div>
              </div>
            ) : null}

            {this.currentPage < 10 ? (
              <div>
                {this.loading ? (
                  <ion-button expand="full">Fetching more movies</ion-button>
                ) : (
                  <ion-button
                    expand="full"
                    onClick={() => {
                      this.getMoreShowingMovies();
                    }}
                  >
                    Load More
                  </ion-button>
                )}
              </div>
            ) : null}
          </div>
          <div class="right" padding>
            <h4>Upcoming Movies</h4>
            {this.upcomingMovies.length > 0 ? (
              <ion-list lines="inset">
                {this.upcomingMovies.map(movie => (
                  <ion-item>
                    <ion-label>
                      {movie.title} <p>{movie.release_date}</p>
                    </ion-label>
                  </ion-item>
                ))}
              </ion-list>
            ) : null}
          </div>
        </div>
      </ion-content>
    );
  }
}
