import { Component, State, Prop } from "@stencil/core";
import DatabaseProvider from "../../services/database";

@Component({
  tag: "imovies-login",
  styleUrl: "imovies-login.scss"
})
export class ImoviesLogin {
  @Prop({ connect: "ion-nav" })
  nav: HTMLIonNavElement;
  @Prop({ connect: "ion-loading-controller" })
  loadingCtrl: HTMLIonLoadingControllerElement;
  @Prop({ connect: "ion-toast-controller" })
  toastCtrl: HTMLIonToastControllerElement;
  private databaseProvider: DatabaseProvider = new DatabaseProvider();
  @State()
  email: string;
  @State()
  password: string;

  resetPassword() {
    if (this.email != null) {
      this.databaseProvider
        .resetPassword(this.email)
        .then(() => {
          this.toastCtrl
            .create({
              message: "Password reset email sent. Check you're email",
              showCloseButton: true,
              position: "top",
              closeButtonText: "Okay",
              cssClass: "toast-success"
            })
            .then(toast => {
              toast.present();
            })
            .catch(error => {
              console.error(error);
            });
        })
        .catch(() => {
          this.toastCtrl
            .create({
              message: "Something went wrong. Please try again",
              showCloseButton: true,
              position: "top",
              closeButtonText: "Okay",
              cssClass: "toast-fail"
            })
            .then(toast => {
              toast.present();
            })
            .catch(error => {
              console.error(error);
            });
        });
    } else {
      this.toastCtrl
        .create({
          message:
            "Please fill in your email,skip password field and then try again",
          showCloseButton: true,
          position: "top",
          closeButtonText: "Okay",
          cssClass: "toast-fail"
        })
        .then(toast => {
          toast.present();
        })
        .catch(error => {
          console.error(error);
        });
    }
  }

  login() {
    if (this.email != null && this.password != null) {
      this.loadingCtrl
        .create({
          spinner: "dots",
          message: "Logging In...",
          translucent: true,
          cssClass: "loader"
        })
        .then(loader => {
          loader.present();
          this.databaseProvider
            .loginWithEmail(this.email, this.password)
            .then(() => {
              loader.dismiss();
              this.nav.componentOnReady().then(navEl => {
                navEl.setRoot("imovies-home");
              });
            })
            .catch(error => {
              loader.dismiss();
              console.error(error);
              if (error == "password") {
                this.toastCtrl
                  .create({
                    message: "Wrong Password",
                    showCloseButton: true,
                    position: "top",
                    closeButtonText: "Okay",
                    cssClass: "toast-fail"
                  })
                  .then(toast => {
                    toast.present();
                  })
                  .catch(error => {
                    console.error(error);
                  });
              } else if (error == "verify") {
                this.toastCtrl
                  .create({
                    message:
                      "Please open your email and verify your registration",
                    showCloseButton: true,
                    position: "top",
                    closeButtonText: "Okay",
                    cssClass: "toast-fail"
                  })
                  .then(toast => {
                    toast.present();
                  })
                  .catch(error => {
                    console.error(error);
                  });
              } else {
                this.toastCtrl
                  .create({
                    message: "There was an error, Please try again",
                    showCloseButton: true,
                    position: "top",
                    closeButtonText: "Okay",
                    cssClass: "toast-fail"
                  })
                  .then(toast => {
                    toast.present();
                  })
                  .catch(error => {
                    console.error(error);
                  });
              }
            });
        })
        .catch(error => {
          console.error(error);
        });
    } else {
      this.toastCtrl
        .create({
          message: "Fill all details before proceeding",
          showCloseButton: true,
          position: "top",
          closeButtonText: "Okay",
          cssClass: "toast-fail"
        })
        .then(toast => {
          toast.present();
        })
        .catch(error => {
          console.error(error);
        });
    }
  }
  render() {
    return (
      <ion-split-pane when="lg">
        <ion-content main>
          <div class="login-form">
            <h4>Login</h4>
            <ion-item>
              <ion-label position="stacked">Email</ion-label>
              <ion-input
                type="email"
                autofocus
                clearInput
                inputmode="email"
                placeholder="Enter Your Email"
                value={this.email}
                onInput={(event: any) => {
                  this.email = event.target.value;
                }}
              />
            </ion-item>
            <ion-item>
              <ion-label position="stacked">Password</ion-label>
              <ion-input
                type="password"
                placeholder="Enter Your Password"
                value={this.password}
                onInput={(event: any) => {
                  this.password = event.target.value;
                }}
              />
            </ion-item>
            <ion-button
              color="primary"
              onClick={() => {
                this.login();
              }}
            >
              Login
            </ion-button>

            <ion-anchor href="/register" routerDirection="forward">
              Don't Have An Account?
            </ion-anchor>
            <p
              onClick={() => {
                this.resetPassword();
              }}
            >
              Forgot Password?
            </p>
          </div>
        </ion-content>
        <div class="sideEf">
          <div class="title">
            <h2>iMovies</h2>
            <p>Movie Booking With Ease</p>
          </div>
        </div>
      </ion-split-pane>
    );
  }
}
