import { Component, Prop, State } from "@stencil/core";
import DatabaseProvider from "../../services/database";

@Component({
  tag: "imovies-start",
  styleUrl: "imovies-start.scss"
})
export class ImoviesStart {
  @Prop({ connect: "ion-nav" })
  nav: HTMLIonNavElement;
  private databaseProvider: DatabaseProvider = new DatabaseProvider();
  @State()
  loggedin: Boolean = false;

  componentWillLoad() {
    this.checkAuth();
  }

  checkAuth() {
    //private databaseProvider: DatabaseProvider = new DatabaseProvider();
    this.databaseProvider.checkLoginStatus().subscribe(
      (user: firebase.User) => {
        this.loggedin = true;
      },
      () => {
        this.loggedin = false;
      }
    );
  }
  render() {
    return (
      <ion-content>
        <div class="top">
          <h2>iMovies</h2>
          <p>Movie Booking With Ease</p>
        </div>
        <div class="bottom">
          <div class="left" padding>
            <p>
              iMovies is your one stop online booking site. Book movie seats
              with ease. Get your tickets sent to your email. No more queues.
              View Movie, Book Seatsand that's it. It couldn't be any easier.
            </p>

            {this.loggedin ? (
              <ion-anchor href="/home" routerDirection="forward">
                Go To Movies
              </ion-anchor>
            ) : (
              <ion-anchor href="/login" routerDirection="forward">
                Go To Login
              </ion-anchor>
            )}
          </div>
          <div class="right">
            <ion-grid>
              <ion-row>
                <ion-col>
                  <img src="../../assets/images/movie1.jpg" alt="" />
                </ion-col>
                <ion-col>
                  <img src="../../assets/images/movie2.jpg" alt="" />
                </ion-col>
                <ion-col>
                  <img src="../../assets/images/movie3.jpg" alt="" />
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <img src="../../assets/images/movie4.jpg" alt="" />
                </ion-col>
                <ion-col>
                  <img src="../../assets/images/movie5.jpg" alt="" />
                </ion-col>
                <ion-col>
                  <img src="../../assets/images/movie6.jpg" alt="" />
                </ion-col>
              </ion-row>
            </ion-grid>
          </div>
        </div>
      </ion-content>
    );
  }
}
