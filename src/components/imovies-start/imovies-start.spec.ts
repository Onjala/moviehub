import { TestWindow } from '@stencil/core/testing';
import { ImoviesStart } from './imovies-start';

describe('imovies-start', () => {
  it('should build', () => {
    expect(new ImoviesStart()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLImoviesStartElement;
    let testWindow: TestWindow;
    beforeEach(async () => {
      testWindow = new TestWindow();
      element = await testWindow.load({
        components: [ImoviesStart],
        html: '<imovies-start></imovies-start>'
      });
    });

    // See https://stenciljs.com/docs/unit-testing
    {cursor}

  });
});
