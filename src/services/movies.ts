import { Observable } from "rxjs";

export default class MoviesProvider {
  API_KEY: string = "4d51e2149ffec1e3fabb84a54d724b76";
  constructor() {}

  getUpcomingMovies(page: number) {
    return new Promise((resolve, reject) => {
      fetch(
        `https://api.themoviedb.org/3/movie/upcoming?api_key=${
          this.API_KEY
        }&language=en-US&page=${page}`
      )
        .then(response => response.json())
        .then(data => {
          resolve(data.results);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  searchMovie(query,page:number){
    return new Promise((resolve,reject)=>{
      fetch(`https://api.themoviedb.org/3/search/movie?api_key=${this.API_KEY}&language=en-US&query=${query}&page=${page}&include_adult=false`)
        .then(response => response.json())
        .then(data => {
          resolve(data.results);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  getShowingMovies(page: number) {
    return new Promise((resolve, reject) => {
      fetch(
        `https://api.themoviedb.org/3/movie/now_playing?api_key=${
          this.API_KEY
        }&language=en-US&page=${page}`
      )
        .then(response => response.json())
        .then(data => {
          resolve(data.results);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  getMovieDetails(movieid: number) {
    return new Promise((resolve, reject) => {
      fetch(
        `https://api.themoviedb.org/3/movie/${movieid}?api_key=${
          this.API_KEY
        }&language=en-US`
      )
        .then(response => response.json())
        .then(data => {
          resolve(data);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  getMovieCast(movieid: number) {
    return new Promise((resolve, reject) => {
      fetch(
        `https://api.themoviedb.org/3/movie/${movieid}/credits?api_key=${
          this.API_KEY
        }`
      )
        .then(response => response.json())
        .then(data => {
          resolve(data.cast);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  getRandomTheatre() {
    var theatres = [
      "iMax Garden City",
      "iMax 20th Century",
      "iMax Anga Sky Pangani",
      "Century Cinema Junction",
      "Westgate Cinema",
      "Prestige Cinema"
    ];

    return theatres;
  }

  getrandomDate() {
    var date = new Date();
    var start = new Date(date.getFullYear(), date.getMonth(), 1);
    return new Date(
      start.getTime() + Math.random() * (date.getTime() - start.getTime())
    );
  }

  getViewingType() {
    var types = ["2d", "3d"];
    return types[Math.floor(Math.random() * types.length)];
  }

  getSeats() {
    return [{ row: "a", columns: [{ column: 1, booked: false }, { column: 2, booked: false }, { column: 3, booked: false }, { column: 4, booked: false }, { column: 5, booked: false }, { column: 6, booked: false }, { column: 7, booked: false }, { column: 8, booked: false }, { column: 9, booked: false }, { column: 10, booked: false }, { column: 11, booked: false }, { column: 12, booked: false }, { column: 13, booked: false }, { column: 14, booked: false }, { column: 15, booked: false }, { column: 16, booked: false }, { column: 17, booked: false }] }, { row: "b", columns: [{ column: 1, booked: false }, { column: 2, booked: false }, { column: 3, booked: false }, { column: 4, booked: false }, { column: 5, booked: false }, { column: 6, booked: false }, { column: 7, booked: false }, { column: 8, booked: false }, { column: 9, booked: false }, { column: 10, booked: false }, { column: 11, booked: false }, { column: 12, booked: false }, { column: 13, booked: false }, { column: 14, booked: false }, { column: 15, booked: false }, { column: 16, booked: false }, { column: 17, booked: false }] }, { row: "c", columns: [{ column: 1, booked: false }, { column: 2, booked: false }, { column: 3, booked: false }, { column: 4, booked: false }, { column: 5, booked: false }, { column: 6, booked: false }, { column: 7, booked: false }, { column: 8, booked: false }, { column: 9, booked: false }, { column: 10, booked: false }, { column: 11, booked: false }, { column: 12, booked: false }, { column: 13, booked: false }, { column: 14, booked: false }, { column: 15, booked: false }, { column: 16, booked: false }, { column: 17, booked: false }] }, { row: "d", columns: [{ column: 1, booked: false }, { column: 2, booked: false }, { column: 3, booked: false }, { column: 4, booked: false }, { column: 5, booked: false }, { column: 6, booked: false }, { column: 7, booked: false }, { column: 8, booked: false }, { column: 9, booked: false }, { column: 10, booked: false }, { column: 11, booked: false }, { column: 12, booked: false }, { column: 13, booked: false }, { column: 14, booked: false }, { column: 15, booked: false }, { column: 16, booked: false }, { column: 17, booked: false }] }, { row: "e", columns: [{ column: 1, booked: false }, { column: 2, booked: false }, { column: 3, booked: false }, { column: 4, booked: false }, { column: 5, booked: false }, { column: 6, booked: false }, { column: 7, booked: false }, { column: 8, booked: false }, { column: 9, booked: false }, { column: 10, booked: false }, { column: 11, booked: false }, { column: 12, booked: false }, { column: 13, booked: false }, { column: 14, booked: false }, { column: 15, booked: false }, { column: 16, booked: false }, { column: 17, booked: false }] }, { row: "f", columns: [{ column: 1, booked: false }, { column: 2, booked: false }, { column: 3, booked: false }, { column: 4, booked: false }, { column: 5, booked: false }, { column: 6, booked: false }, { column: 7, booked: false }, { column: 8, booked: false }, { column: 9, booked: false }, { column: 10, booked: false }, { column: 11, booked: false }, { column: 12, booked: false }, { column: 13, booked: false }, { column: 14, booked: false }, { column: 15, booked: false }, { column: 16, booked: false }, { column: 17, booked: false }] }, { row: "g", columns: [{ column: 1, booked: false }, { column: 2, booked: false }, { column: 3, booked: false }, { column: 4, booked: false }, { column: 5, booked: false }, { column: 6, booked: false }, { column: 7, booked: false }, { column: 8, booked: false }, { column: 9, booked: false }, { column: 10, booked: false }, { column: 11, booked: false }, { column: 12, booked: false }, { column: 13, booked: false }, { column: 14, booked: false }, { column: 15, booked: false }, { column: 16, booked: false }, { column: 17, booked: false }] }, { row: "h", columns: [{ column: 1, booked: false }, { column: 2, booked: false }, { column: 3, booked: false }, { column: 4, booked: false }, { column: 5, booked: false }, { column: 6, booked: false }, { column: 7, booked: false }, { column: 8, booked: false }, { column: 9, booked: false }, { column: 10, booked: false }, { column: 11, booked: false }, { column: 12, booked: false }, { column: 13, booked: false }, { column: 14, booked: false }, { column: 15, booked: false }, { column: 16, booked: false }, { column: 17, booked: false }] }, { row: "i", columns: [{ column: 1, booked: false }, { column: 2, booked: false }, { column: 3, booked: false }, { column: 4, booked: false }, { column: 5, booked: false }, { column: 6, booked: false }, { column: 7, booked: false }, { column: 8, booked: false }, { column: 9, booked: false }, { column: 10, booked: false }, { column: 11, booked: false }, { column: 12, booked: false }, { column: 13, booked: false }, { column: 14, booked: false }, { column: 15, booked: false }, { column: 16, booked: false }, { column: 17, booked: false }] }];
  }
}
