import firebase from "firebase";
import { Observable } from "rxjs";

export default class DatabaseProvider {
  constructor() {}
  db = firebase.firestore();

  registerWithEmail(email, password, name, username) {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(() => {
          //let userdata = JSON.parse(JSON.stringify(response));
          firebase
            .auth()
            .currentUser.sendEmailVerification()
            .then(() => {
              this.createUser(
                name,
                username,
                "https://profile.actionsprout.com/default.jpeg"
              )
                .then(() => {
                  resolve(true);
                })
                .catch(err => {
                  console.error(err);
                  this.deleteAccount()
                    .then(() => {
                      reject(false);
                    })
                    .catch(err => {
                      console.error(err);
                    });
                });
            })
            .catch(err => {
              console.error(err);
              this.deleteAccount()
                .then(() => {
                  reject(false);
                })
                .catch(err => {
                  console.error(err);
                });
            });
        })
        .catch(err => {
          console.error(err);
          reject("email");
        });
    });
  }

  loginWithEmail(email, password) {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(() => {
          // let userdata = JSON.parse(JSON.stringify(response));
          // if (userdata.user.emailVerified === true) {
          //   resolve(true);
          // } else {
          //   reject("verify");
          // }
          resolve(true);
        })
        .catch(err => {
          if (err.code === "auth/wrong-password") {
            reject("password");
          } else {
            console.error(err.code);
            reject(false);
          }
        });
    });
  }

  createUser(name, username, displaypic) {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .currentUser.updateProfile({
          displayName: name,
          photoURL: displaypic
        })
        .then(() => {
          this.db
            .collection("users")
            .doc(firebase.auth().currentUser.uid)
            .set({
              uid: firebase.auth().currentUser.uid,
              displayName: name,
              userName: username,
              profilephoto: displaypic
            })
            .then(() => {
              resolve(true);
            })
            .catch(err => {
              console.error(err);
              reject(false);
            });
        })
        .catch(err => {
          console.error(err);
          reject(false);
        });
    });
  }

  getUserDetails() {
    return new Observable(observer => {
      this.checkLoginStatus().subscribe((user: firebase.User) => {
        this.db
          .collection("users")
          .doc(user.uid)
          .onSnapshot(
            doc => {
              observer.next(doc.data());
            },
            err => {
              observer.error(false);
            }
          );
      });
    });
  }

  getBookedSeats(movie, theatre, time, date) {
    return new Observable(observer => {
      this.db
        .collection("bookings")
        .doc(movie)
        .collection("dates")
        .doc(date)
        .collection("theatres")
        .doc(theatre)
        .collection("times")
        .doc(time)
        .collection("seats")
        .onSnapshot(
          res => {
            observer.next(res.docs.map(doc => doc.data()));
          },
          err => {
            observer.error(err);
          }
        );
    });
  }

  bookMovie(movie, theatre, time, date, seats: Array<string>) {
    return new Promise((resolve, reject) => {
      try {
        this.db
          .collection("bookings")
          .doc(movie)
          .set({ movieid: movie })
          .then(() => {
            this.db
              .collection("bookings")
              .doc(movie)
              .collection("dates")
              .doc(date)
              .set({ date: date })
              .then(() => {
                this.db
                  .collection("bookings")
                  .doc(movie)
                  .collection("dates")
                  .doc(date)
                  .collection("theatres")
                  .doc(theatre)
                  .set({ theatre: theatre })
                  .then(() => {
                    this.db
                      .collection("bookings")
                      .doc(movie)
                      .collection("dates")
                      .doc(date)
                      .collection("theatres")
                      .doc(theatre)
                      .collection("times")
                      .doc(time)
                      .set({ time: time })
                      .then(() => {
                        for (const seat of seats) {
                          this.db
                            .collection("bookings")
                            .doc(movie)
                            .collection("dates")
                            .doc(date)
                            .collection("theatres")
                            .doc(theatre)
                            .collection("times")
                            .doc(time)
                            .collection("seats")
                            .doc(seat)
                            .set({ seat: seat });
                        }
                        this.checkLoginStatus().subscribe(
                          (user: firebase.User) => {
                            this.db
                              .collection("bookings")
                              .doc(movie)
                              .collection("dates")
                              .doc(date)
                              .collection("theatres")
                              .doc(theatre)
                              .collection("times")
                              .doc(time)
                              .collection("clients")
                              .add({
                                email: user.email,
                                name: user.displayName,
                                seats: seats
                              })
                              .then(ref => {
                                this.db
                                  .collection("users")
                                  .doc(user.uid)
                                  .collection("booked")
                                  .doc(ref.id)
                                  .set({
                                    id: ref.id,
                                    movie: movie,
                                    date: date,
                                    time: time,
                                    theatre: theatre,
                                    seats: seats
                                  });
                              });
                          }
                        );
                        resolve(true);
                      });
                  });
              });
          });
      } catch (error) {
        reject(error);
      }
    });
  }

  logout() {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .signOut()
        .then(() => {
          resolve(true);
        })
        .catch(err => {
          reject(err);
          console.error(err);
        });
    });
  }

  resetPassword(email) {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .sendPasswordResetEmail(email)
        .then(() => {
          resolve(true);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  deleteAccount() {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .currentUser.delete()
        .then(() => {
          resolve(true);
        })
        .catch(err => {
          reject(err);
          console.error(err);
        });
    });
  }

  checkLoginStatus() {
    return new Observable(observer => {
      firebase.auth().onAuthStateChanged(user => {
        if (user) {
          // User is signed in.
          observer.next(user);
        } else {
          // No user is signed in.
          observer.error(false);
        }
      });
    });
  }
}
